<div align="center"><img width="500" alt="Logo automatizAI" src="https://automatizai.com.br/assets/img/automatizai-logo.svg"></div>
<br>
<br>
<br>

<h1 align="center">  DESAFIO FRONTEND AUTOMATIZAI</h1>

Para se tornar um camaleão frontend você precisa saber como posicionar elementos corretamente na tela, dado o template a cima, clone-o em sua maquina e faça-o parecer com a imagem abaixo, não se preocupe com as cores, elas já foram linkadas

### Objetivo
![Objetivo](assets/desafio2.png "Objetivo a cumprir.")


### Dê o seu melhor e boa sorte.
<br>
<br>
<br>
<h1 align="center"> AUTOMATIZAI 2021</h1>
<h1 align="center"> CAMALEÕES DA TECNOLOGIA</h1>

```
                      _       _._
               _,,-''' ''-,_ }'._''.,_.=._
            ,-'      _ _    '        (  @)'-,
          ,'  _..==;;::_::'-     __..----'''}
         :  .'::_;==''       ,'',: : : '' '}
        }  '::-'            /   },: : : :_,'
       :  :'     _..,,_    '., '._-,,,--\'    _
      :  ;   .-'       :      '-, ';,__\.\_.-'
     {   '  :    _,,,   :__,,--::',,}___}^}_.-'
     }        _,'__''',  ;_.-''_.-'
    :      ,':-''  ';, ;  ;_..-'
_.-' }    ,',' ,''',  : ^^
_.-''{    { ; ; ,', '  :
      }   } :  ;_,' ;  }
       {   ',',___,'   '
        ',           ,'
          '-,,__,,-'
```
